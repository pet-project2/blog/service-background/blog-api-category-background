﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Message<T>
    {
        public MessageSchema Schema { get; set; }
        public MessagePayload<T> Payload { get; set; }
    }
}
