﻿using System;

namespace Model
{
    public class Schema
    {
        public string Type { get; set; }
        public object Fields { get; set; }
        public bool Optional { get; set; }
        public string Name { get; set; }
    }
}
