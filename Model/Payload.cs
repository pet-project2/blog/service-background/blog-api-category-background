﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Payload<T>
    {
        public T Before { get; set; }
        public T After { get; set; }
        public Source Source { get; set; }
        public string Op { get; set; }

        [JsonProperty(PropertyName = "ts_ms")]
        public string Tsms { get; set; }
        public object Transaction { get; set; }
    }
}
