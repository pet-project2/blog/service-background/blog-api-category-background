﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Source
    {
        public string Version { get; set; }
        public string Connector { get; set; }
        public string Name { get; set; }

        [JsonProperty(PropertyName = "ts_ms")]
        public string Tsms { get; set; }
        public bool Snapshot { get; set; }
        public string Bb { get; set; }
        public string Schema { get; set; }
        public string Table { get; set; }

        [JsonProperty(PropertyName = "change_lsn")]
        public string Changelsn { get; set; }

        [JsonProperty(PropertyName = "commit_lsn")]
        public string Commit_lsn { get; set; }

        [JsonProperty(PropertyName = "event_serial_no")]
        public int EventSerialNo { get; set; }
    }
}
