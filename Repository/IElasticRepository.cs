﻿using Elasticsearch.Net;
using System;
using System.Threading.Tasks;

namespace Repository
{
    public interface IElasticRepository<T>
    {
        public Task<IApiCallDetails> CreateDocumentAsync(T document, string indexName = "");
        public Task<IApiCallDetails> UpdateDocumentAsync(T document, string indexName = "");
        public Task<IApiCallDetails> DeleteDocumentAsync(Guid id, string indexName = "");
        public Task<T> GetById(Guid id, string indexName = "");
    }
}
