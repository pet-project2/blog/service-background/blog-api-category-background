﻿using Domain;
using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class CategoryRepository : IElasticRepository<Category>
    {
        private readonly IElasticClient _esClient;
        public CategoryRepository(IElasticClient esClient)
        {
            this._
        }
        public Task<IApiCallDetails> CreateDocumentAsync(Category document, string indexName = "")
        {
            throw new NotImplementedException();
        }

        public Task<IApiCallDetails> DeleteDocumentAsync(Guid id, string indexName = "")
        {
            throw new NotImplementedException();
        }

        public Task<Category> GetById(Guid id, string indexName = "")
        {
            throw new NotImplementedException();
        }

        public Task<IApiCallDetails> UpdateDocumentAsync(Category document, string indexName = "")
        {
            throw new NotImplementedException();
        }
    }
}
