﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using System;

namespace Infrastructure
{
    public static class ElasticsearchConfig
    {
        public static void ConnectElasticSearch(this IServiceCollection services, IConfiguration configuration)
        {
            var url = configuration["elasticsearch:url"];
            var defaultIndex = configuration["elasticsearch:index"];

            var settings = new ConnectionSettings(new Uri(url)).DefaultIndex(defaultIndex);

            var clients = new ElasticClient(settings);
            services.AddSingleton<IElasticClient>(clients);
        }
    }
}
