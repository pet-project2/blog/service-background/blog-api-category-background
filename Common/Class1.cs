﻿using System;

namespace Common
{
    public struct Constants
    {
        public static string Create = "C";
        public static string Update = "U";
        public static string Delete = "D";
    }
}
