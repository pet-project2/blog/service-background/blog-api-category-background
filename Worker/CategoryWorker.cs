using Common;
using Confluent.Kafka;
using Domain;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Worker
{
    public class CategoryWorker : BackgroundService
    {
        private readonly ILogger<CategoryWorker> _logger;

        public CategoryWorker(ILogger<CategoryWorker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var config = new ConsumerConfig
            {
                BootstrapServers = "192.168.1.123:9092",
                GroupId = "Consume1",
                AutoOffsetReset = AutoOffsetReset.Earliest,
                EnableAutoCommit = false
            };

            var topic = "TOMMY.dbo.Categories";

            using (var consumer = new ConsumerBuilder<string, string>(config).Build())
            {
                consumer.Subscribe(topic);
                var count = 0;
                while (!stoppingToken.IsCancellationRequested)
                {
                    _logger.LogInformation("Waiting Message --- {time}", DateTimeOffset.Now);
                    var consumeResult = consumer.Consume(stoppingToken);
                    var json = JsonConvert.DeserializeObject<Message<Category>>(consumeResult.Message.Value);
                    if(json != null)
                    {
                        // handle 
                        _logger.LogInformation($"Message Value is : {json.Payload.After}");
                    }
                    consumer.Commit();
                    _logger.LogInformation($"Topic Poition {count}");
                    count++;
                    await Task.Delay(1000, stoppingToken);
                }
            }
        }

        //private Task<Category> GetCategoryByMethod(string method)
        //{
        //    switch(method)
        //    {
        //        case Constants.Create:
                     
        //    }
        //}
    }
}
